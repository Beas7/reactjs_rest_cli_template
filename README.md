# ReactJS_REST_Cli_Template

React initial template created with: react-create-app

Simple React JS client for REST API example, only a single component exposed to the main app


**Run Instructions:**

<pre>
 cd ./app
 yarn install
 yarn start
</pre>


*More info inside ./app/README.md