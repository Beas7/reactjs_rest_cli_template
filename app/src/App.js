import React, {Component} from 'react';
import SwapiPlanets from './components/swapiPlanets';

class App extends Component {

  state = {
    planets: []
  }

  componentDidMount() {
    fetch('https://swapi.dev/api/planets/')
    .then(res => res.json())
    .then((data) => {
      this.setState({ planets: data.results })
    })
    .catch(console.log)
  }

  render () {
    return (
      <SwapiPlanets planets={this.state.planets} />
    );
  }
}

export default App;
