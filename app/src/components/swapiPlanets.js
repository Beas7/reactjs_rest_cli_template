import React from 'react'

    const SwapiPlanets = ({ planets }) => {
      return (
        <div>
          <center><h1>SW Planets</h1></center>
          <div className="planetsWrapper">
            {planets.map((planet) => (
                <div className="card" key={planet.diameter}>
                <p className="name"><span className="label">Planet: </span> {planet.name}</p>
                {planet.population === 'unknown' 
                ? <p className="unknown"><span className="label">Population: </span> {planet.population}</p> 
                : <p className="population"><span className="label">Population: </span> {planet.population}</p>}
                </div>
            ))}
          </div>
        </div>
      )
    };

    export default SwapiPlanets